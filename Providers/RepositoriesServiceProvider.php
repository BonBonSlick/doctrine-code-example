<?php

declare(strict_types=1);

namespace App\Providers;

use App\Entity\Article;
use App\Entity\User;
use App\Repository\ArticlesRepository;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManager;
use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoriesServiceProvider
 */
class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    private $defaults = [
        UsersRepository::class    => User::class,
        ArticlesRepository::class => Article::class,
    ];

    /**
     * @return void
     */
    public function boot(): void
    {
        $manager = $this->app->make(EntityManager::class);

        foreach ($this->defaults as $interface => $entity) {
            $this->app->singleton($interface, function () use ($entity, $manager) {
                return $manager->getRepository($entity);
            });
        }
    }
}
