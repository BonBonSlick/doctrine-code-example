<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Interface UsersRepository
 */
interface UsersRepository extends ObjectRepository
{

}
