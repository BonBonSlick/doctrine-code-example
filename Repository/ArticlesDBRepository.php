<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Article;
use Doctrine\ORM\EntityRepository;

/**
 * Class ArticlesDBRepository
 */
class ArticlesDBRepository extends EntityRepository implements ArticlesRepository
{
    /**
     * @param string $url
     * @param bool $onlyEnabled
     * @return Article|null|object
     * @throws \RuntimeException
     */
    public function findByUrl(string $url, bool $onlyEnabled = true): ?Article
    {
        $criterion = ['url' => $url];

        if ($onlyEnabled) {
            $criterion['status'] = Article\Status::ACTIVE;
        }

        return $this->findOneBy($criterion);
    }
}
