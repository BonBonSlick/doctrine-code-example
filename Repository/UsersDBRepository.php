<?php
declare(strict_types=1);

namespace App\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UsersDBRepository
 */
class UsersDBRepository extends EntityRepository implements UsersRepository
{

}
