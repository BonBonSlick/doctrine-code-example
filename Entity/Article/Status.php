<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\Article;

use App\DBAL\Types\Enum;

/**
 * Class Status
 */
class Status extends Enum
{
    public const ACTIVE = 'Active';
    public const NOT_ACTIVE = 'NotActive';
}
