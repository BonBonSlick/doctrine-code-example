<?php

declare(strict_types=1);

namespace App\Entity\Article;

use App\Entity\Article\Content\Renderer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable()
 */
class Content
{
    /**
     * @var string
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(name="content_rendered", type="text")
     */
    private $rendered;

    /**
     * Content constructor.
     * @param string $original
     * @param Renderer $renderer
     */
    public function __construct(string $original, Renderer $renderer)
    {
        $this->update($original, $renderer);
    }

    /**
     * @param string $text
     * @param Renderer $renderer
     * @return Content
     */
    public function update(string $text, Renderer $renderer): self
    {
        $this->content = $text;
        $this->rendered = $renderer->render($text);

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->rendered;
    }

    /**
     * @return string
     */
    public function getOriginalContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getContent();
    }
}
