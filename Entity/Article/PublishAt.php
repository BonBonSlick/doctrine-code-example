<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\Common;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait PublishAt
 */
trait PublishAt
{
    /**
     * @var \DateTimeInterface|Carbon
     * @ORM\Column(name="publish_at", type="carbon")
     */
    protected $published;

    /**
     * Get published
     * @return \DateTimeInterface|Carbon
     */
    public function getPublished(): Carbon
    {
        return $this->published ?? ($this->published = Carbon::now());
    }

    /**
     * @ORM\PrePersist
     */
    public function onSavePublishAt(): void
    {
        if ($this->published === null) {
            $this->published = Carbon::now();
        }
    }
}
