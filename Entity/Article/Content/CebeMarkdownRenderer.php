<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\Article\Content;

use cebe\markdown\Parser;

/**
 * Class CebeMarkdownRenderer
 */
class CebeMarkdownRenderer implements Renderer
{
    /**
     * @var Parser
     */
    private $parser;

    /**
     * @var array
     */
    private $before = [
        '^\?>' => "\n" . '> {tip}',
        '^\!>' => "\n" . '> {note}',
    ];

    /**
     * @var array
     */
    private $after = [
        '-&gt;'                  => '→',
        '\h+\-\h+'               => ' — ',
        '<blockquote><p>{(.*?)}' => '<blockquote class="$1"><p>',
        '<p><img\h*src="(.*?)"\h*alt="(.*?)"\h*\/?><\/p>' => '<img src="$1" alt="$2" />'
    ];

    /**
     * CebeMarkdownRenderer constructor.
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param string $content
     * @return string
     */
    public function render(string $content): string
    {
        $content = $this->before($content);

        $content = $this->parser->parse($content);

        $content = $this->after($content);

        return $content;
    }

    /**
     * @param string $content
     * @return string
     */
    private function before(string $content): string
    {
        foreach ($this->before as $lexeme => $replacedBy) {
            $pattern = \sprintf('/%s/um', $lexeme);
            $content = \preg_replace($pattern, $replacedBy, $content);
        }

        return $content;
    }

    /**
     * @param string $content
     * @return string
     */
    private function after(string $content): string
    {
        foreach ($this->after as $lexeme => $replacedBy) {
            $pattern = \sprintf('/%s/um', $lexeme);
            $content = \preg_replace($pattern, $replacedBy, $content);
        }

        return $content;
    }
}
