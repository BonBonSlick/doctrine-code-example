<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity;

use App\Entity\Article\Content;
use App\Entity\Article\Status;
use App\Entity\Common\PublishAt;
use App\Entity\Common\Timestamps;
use App\Repository\ArticlesDBRepository;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Support\Str;

/**
 * @ORM\Entity(repositoryClass=ArticlesDBRepository::class)
 * @ORM\Table(name="articles", indexes={
 *      @ORM\Index(name="articles_url_unique", columns={
 *          "url"
 *      })
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class Article extends BaseEntity
{
    use PublishAt;
    use Timestamps;

    /**
     * @var string
     * @ORM\Column(name="url", type="string")
     */
    private $url;

    /**
     * @var string
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @var Content
     * @ORM\Embedded(class=Content::class, columnPrefix=false)
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(name="status", type=Status::class)
     */
    private $status = Status::NOT_ACTIVE;

    /**
     * Article constructor.
     * @param string $title
     * @param Content $content
     */
    public function __construct(string $title, Content $content)
    {
        $this->title = Str::title($title);
        $this->content = $content;
        $this->url = $this->getCreated()->format('Y/m/d/') . \str_slug($title);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $title
     * @return Article
     */
    public function rename(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Article
     */
    public function enable(): self
    {
        $this->status = Status::ACTIVE;

        return $this;
    }

    /**
     * @return Article
     */
    public function disable(): self
    {
        $this->status = Status::NOT_ACTIVE;

        return $this;
    }

    /**
     * @return Content
     */
    public function getBody(): Content
    {
        return $this->content;
    }
}
