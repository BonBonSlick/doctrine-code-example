<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Common\Timestamps;
use App\Entity\User\Authenticatable;
use App\Entity\User\Authorizable;
use App\Entity\User\CanResetPassword;
use App\Entity\User\Credentials;
use App\Repository\UsersDBRepository;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Contracts\Auth\Access\Authorizable as HasAuthorization;
use Illuminate\Contracts\Auth\Authenticatable as HasAuthentication;
use Illuminate\Contracts\Auth\CanResetPassword as HasPasswordResetting;

/**
 * @ORM\Entity(repositoryClass=UsersDBRepository::class)
 * @ORM\Table(name="users", indexes={
 *     @ORM\Index(name="users_email_unique", columns={
 *          "credentials.email"
 *     })
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseEntity implements HasAuthentication, HasAuthorization, HasPasswordResetting
{
    use Timestamps;
    use Authorizable;
    use Authenticatable;
    use CanResetPassword;

    /**
     * @var string
     * @ORM\Column(name="remember_token", type="string", nullable=true)
     */
    protected $rememberToken;

    /**
     * @var Credentials
     * @ORM\Embedded(class=Credentials::class, columnPrefix=false)
     */
    private $credentials;

    /**
     * User constructor.
     * @param Credentials $credentials
     */
    public function __construct(Credentials $credentials)
    {
        $this->credentials = $credentials;
        $this->created     = Carbon::now();
    }

    /**
     * @return Credentials
     */
    public function getCredentials(): Credentials
    {
        return $this->credentials;
    }
}
