<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\Common;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait CreatedAt
 */
trait CreatedAt
{
    /**
     * @var \DateTimeInterface|Carbon
     * @ORM\Column(name="created_at", type="carbon")
     */
    protected $created;

    /**
     * Get created
     * @return \DateTimeInterface|Carbon
     */
    public function getCreated(): Carbon
    {
        return $this->created ?? ($this->created = Carbon::now());
    }

    /**
     * @ORM\PrePersist
     */
    public function onSaveCreatedAt(): void
    {
        $this->created = Carbon::now();
    }
}
