<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\Common;

use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait UpdatedAt
 */
trait UpdatedAt
{
    /**
     * @var \DateTimeInterface|Carbon
     * @ORM\Column(name="updated_at", type="carbon", nullable=true)
     */
    protected $updated;

    /**
     * Get updated
     * @return \DateTimeInterface|Carbon
     */
    public function getUpdated(): Carbon
    {
        return $this->updated ?? ($this->updated = Carbon::now());
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @return void
     */
    public function onChangeUpdatedAt(): void
    {
        $this->updated = Carbon::now();
    }
}
