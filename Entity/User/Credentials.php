<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 */
class Credentials
{
    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="email", type="string")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="password", type="string")
     */
    private $password;

    /**
     * Credentials constructor.
     * @param string $name
     * @param string $email
     * @param string $password
     */
    public function __construct(string $name, string $email, string $password)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $this->secure($password);
    }

    /**
     * @param string $string
     * @return string
     */
    private function secure(string $string): string
    {
        return \bcrypt($string);
    }

    /**
     * @param string $name
     * @return Credentials
     */
    public function rename(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $password
     * @return Credentials
     */
    public function updatePassword(string $password): self
    {
        $this->password = $this->secure($password);

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
