<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\User;

use App\Entity\User;
use Illuminate\Contracts\Auth\Authenticatable as HasAuthentication;

/**
 * Trait Authentication
 * @mixin HasAuthentication
 */
trait Authenticatable
{
    /**
     * @return string
     */
    public function getAuthIdentifierName(): string
    {
        return 'id';
    }

    /**
     * @return null|int
     */
    public function getAuthIdentifier(): ?int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getAuthPassword(): string
    {
        /** @var User $this */
        return $this->getCredentials()->getPassword();
    }

    /**
     * @return null|string
     */
    public function getRememberToken(): ?string
    {
        return $this->rememberToken;
    }

    /**
     * @param string $value
     * @return self
     */
    public function setRememberToken($value): self
    {
        $this->rememberToken = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getRememberTokenName(): string
    {
        return 'rememberToken';
    }
}
