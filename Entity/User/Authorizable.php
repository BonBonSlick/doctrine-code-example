<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\User;

use Illuminate\Contracts\Auth\Access\Authorizable as HasAuthorization;
use Illuminate\Foundation\Auth\Access\Authorizable as Base;

/**
 * Trait Authorizable
 * @mixin HasAuthorization
 */
trait Authorizable
{
    use Base;
}
