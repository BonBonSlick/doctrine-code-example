<?php
/**
 * This file is part of serafimarts.ru package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
declare(strict_types=1);

namespace App\Entity\User;

use App\Entity\User;
use Illuminate\Auth\Passwords\CanResetPassword as Base;
use Illuminate\Contracts\Auth\CanResetPassword as HasPasswordResetting;

/**
 * Trait CanResetPassword
 * @mixin HasPasswordResetting
 */
trait CanResetPassword
{
    use Base;

    /**
     * @return string
     */
    public function getEmailForPasswordReset(): string
    {
        /** @var User $this */
        return $this->getCredentials()->getEmail();
    }
}
